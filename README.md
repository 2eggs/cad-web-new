## 环境搭建

环境已经搭建好了，拉取项目后运行`yarn`即可下载所有的包，以下命令是表示已经安装了相应的包或者完成了相应的初始化工作

```
npx create-react-app . --template typescript

yarn add @material-ui/core

yarn add @material-ui/lab

yarn add @material-ui/icons

yarn add urql graphql

yarn add -D @graphql-codegen/cli

yarn graphql-codegen init   // 初始化

yarn add -D @graphql-codegen/typescript-urql

// 路由
yarn add react-router-dom

yarn add @types/react-router-dom -D

// base64
yarn add base-64

yarn add @types/base-64 -D
```
## 生成ulql

写完schema后运行

```
yarn generate
```

注意：运行上一条命令之前，请记得修改以下两处改为自己的ip地址：

1. codegen.yml

```
schema: "http://192.168.144.129:8080/query"
```

2. ./src/App.tsx

```
// 声明 client 实例
const client = createClient({
  url: "http://192.168.144.129:8080/query",
});
```

## 编码注意事项

companyID需要用base64编码

## VSCODE可能会出现的错误

1. "Visual Studio Code is unable to watch for file changes in this large workspace" (error ENOSPC)

解决方案：

```
在 /etc/sysctl.conf 的末尾添加 fs.inotify.max_user_watches=524288

然后执行 sudo sysctl -p
```

## 测试案例查询

1. 投资组件测试案例查询

SELECT * from training_camp.companies c inner join training_camp.investments i on c.permalink = i.investor_permalink 