import React, { useEffect, useState } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Grid from "@material-ui/core/Grid";
import { Box } from "@material-ui/core";
import Paper from '@material-ui/core/Paper';
import Pagination from '@material-ui/lab/Pagination';
import { useFindFinancingInvestorListQuery } from '../generated/graphql';
import LinktoCompanyPage from "./LinktoCompanyPage";
import { useStyles } from "./componentStyle"
import Base64 from "base-64";

interface FundedInvestor {
    first: number,
    after: number,
    id: string,
}

function createData(
    companyID: string,
    companyName: string,
    categoryCode: string,
) {
    return {
        companyID,
        companyName,
        categoryCode
    };
}

interface FundedInvestorProps {
    id: string,
    type: string,
    at: string,
    usd: number,
}

interface Investor {
    companyID: string;
    companyName: string;
    categoryCode: string;
}

export default function FundedInvestor(props: FundedInvestorProps) {
    const classes = useStyles();
    const [rows, setRows] = useState<Investor[]>([]);
    const [newAfter, setNewAfter] = useState(0);
    const [isExist, setIsExist] = useState(true)

    const [page, setPage] = React.useState(1);
    const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
        setPage(value);
    };
    const [totalPageCount, setTotalPageCount] = useState(0);
    const [
        findFinancingInvestorListQueryResult,
        executeFindFinancingInvestorListQuery,
    ] = useFindFinancingInvestorListQuery({
        variables: {
            first: 10,
            after: newAfter,
            id: Base64.decode(props.id),
            type: props.type,
            at: props.at,
            usd: props.usd,
        },
        pause: false,
    });

    useEffect(() => {
        setTimeout(() => {
            setNewAfter((page - 1) * 10);
        }, 0);
    }, [page]);

    useEffect(() => {
        let nodes =
            findFinancingInvestorListQueryResult.data?.QueryFinancingInvestorListByFinancingIndex?.nodes

        let r =
            findFinancingInvestorListQueryResult.data?.QueryFinancingInvestorListByFinancingIndex;
        if (r === null) {
            setIsExist(false)
        }

        let res = nodes?.map((value) => {
            let investor = createData(
                value?.companyInfo?.companyID as string,
                value?.companyInfo?.companyName as string,
                value?.companyInfo?.categoryCode as string,
            );
            return investor;
        });

        while (res?.length as number < 5) {
            res?.push(createData("", "", ""))
        }

        while (res?.length as number > 5 && res?.length as number < 10) {
            res?.push(createData("", "", ""))
        }

        setRows(res as Investor[]);

        let totalCount =
            findFinancingInvestorListQueryResult.data?.QueryFinancingInvestorListByFinancingIndex
                ?.totalCount;
        setTotalPageCount(Math.ceil(totalCount / 10));
    }, [findFinancingInvestorListQueryResult]);

    return (
        <div className={isExist === true ? classes.root : classes.noDisplay}>
            <Grid container>
                <Grid item xs={12} className={classes.container_investment}>
                    <Grid item xs={12} className={classes.investment}>
                        <Box className={classes.financing_title}>投资方</Box>
                        <TableContainer component={Paper} style={{ boxShadow: "none" }}>
                            <Table className={classes.table} aria-label="simple table" style={{ boxShadow: "none" }}>
                                <TableHead style={{ background: "#FAFAFA" }}>
                                    <TableRow>
                                        <TableCell className={classes.investment_object_style} align="left">投资方</TableCell>
                                        <TableCell className={classes.company_category_style} align="left">企业类别</TableCell>
                                        <TableCell style={{ width: "700px" }} align="right" ></TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {rows?.map((row, index) => (
                                        <TableRow key={row.companyID}>

                                            <TableCell className={classes.investment_object_style} align="left">
                                                <LinktoCompanyPage
                                                    key={index}
                                                    companyID={row.companyID}
                                                    companyName={row.companyName}
                                                ></LinktoCompanyPage>
                                            </TableCell>
                                            <TableCell className={classes.company_category_style} align="left">{row.categoryCode == null ? '---' : row.categoryCode}</TableCell>
                                            <TableCell style={{ width: "700px" }} align="right" ></TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                            <Box className={classes.pagination}>
                                <Pagination
                                    count={totalPageCount}
                                    page={page}
                                    shape="rounded"
                                    onChange={handleChange}
                                    siblingCount={1}
                                    boundaryCount={1}
                                />
                            </Box>
                        </TableContainer>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    )
}