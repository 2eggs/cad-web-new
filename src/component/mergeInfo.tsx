import React, { useEffect, useState } from "react";
import Grid from "@material-ui/core/Grid";
import { Box } from "@material-ui/core";
import { useStyles } from "./componentStyle"
import { useFindMergerInfomationQuery } from "../generated/graphql";
import LinktoCompanyPage from "./LinktoCompanyPage";
import Base64 from "base-64";


interface MergeInfoProps {
    id: string
}

export default function MergeInfo(props: MergeInfoProps) {
    const classes = useStyles();
    const [acquiredAt, setAcquiredAt] = useState("");
    const [priceAmount, setPriceAmount] = useState(0)
    const [companyID, setCompanyID] = useState("")
    const [companyName, setCompanyName] = useState("")
    const [isExist, setIsExist] = useState(true)


    const [findMergerInfomationQueryResult] = useFindMergerInfomationQuery({
        variables: {
            id: Base64.decode(props.id)
        },
    })

    useEffect(() => {
        let res = findMergerInfomationQueryResult.data?.QueryMergerInformationByCompanyID;

        setAcquiredAt(res?.acquiredAt as string)
        setCompanyID(res?.acquirerInfo?.companyID as string)
        setCompanyName(res?.acquirerInfo?.companyName as string)
        setPriceAmount(res?.priceAmount)

        if (res === null) {
            setIsExist(false)
        }

    }, [findMergerInfomationQueryResult])

    return (
        <div>
            <Grid item xs={12} className={isExist === true ? classes.container_mergeinfo : classes.noDisplay}>
                <Grid item xs={12} className={classes.overview}>
                    <Box className={classes.overview_title}>兼并信息</Box>
                    <Box
                        className={classes.merge_info}
                        display="flex"
                        flexDirection="row"
                    >
                        <Box className={classes.infoleft} display="flex" flexDirection="row">
                            <Box className={classes.info_left1}>
                                <Box className={classes.infotext}>收购方</Box>
                                <Box className={classes.infotext}>收购日期</Box>
                                <Box className={classes.infotext}>价格</Box>
                            </Box>
                            <Box className={classes.info_left2}>
                                <Box className={classes.infotext}>
                                    <LinktoCompanyPage
                                        companyID={companyID}
                                        companyName={companyName as string}
                                    ></LinktoCompanyPage>
                                </Box>
                                <Box className={classes.infotext}>{acquiredAt}</Box>
                                <Box className={classes.infotext}>{priceAmount}</Box>
                            </Box>
                        </Box>
                    </Box>
                </Grid>
            </Grid>
        </div>
    )
}