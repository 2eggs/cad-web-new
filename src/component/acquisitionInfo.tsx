import React, { useEffect, useState } from "react";
import Grid from "@material-ui/core/Grid";
import { Box } from "@material-ui/core";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import { useStyles } from "./componentStyle";
import Pagination from "@material-ui/lab/Pagination";
import Paper from "@material-ui/core/Paper";
import Base64 from "base-64";
import LinktoCompanyPage from "./LinktoCompanyPage";
import { useFindAcquisitionListQuery } from "../generated/graphql";

interface AcquisitionInfoProps {
  id: string;
}

interface AcquisitionInfo {
  companyID: string;
  companyName: string;
  categoryCode: string;
  priceAmount: number;
  acquiredAt: string;
}

export default function AcquisitionInfo(props: AcquisitionInfoProps) {
  const classes = useStyles();
  const [newFirst, setNewFirst] = useState(10);
  const [newAfter, setNewAfter] = useState(0);
  const [rows, setRows] = useState<AcquisitionInfo[]>([]);
  const [page, setPage] = React.useState(1);
  const [totalPageCount, setTotalPageCount] = useState(0);
  const [rounds, setRounds] = useState(0);
  const [isExist, setIsExist] = useState(true)
  const [emptySpaceCount, setEmptySpaceCount] = useState(0)
  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
  };

  function tranNumber(num: number | undefined, point: number) {
    if (typeof num === "undefined" || num === 0) {
      return "-"
    }
    let n = num as number + ""
    let numStr = n.toString().split('.')[0]
    if (numStr.length < 5) {
      console.log(numStr);
      return numStr;
    } else if (numStr.length >= 5 && numStr.length <= 8) {
      let decimal = numStr.substring(numStr.length - 4, numStr.length - 4 + point)
      return Math.floor(num / 10000) + '.' + decimal + '万'
    } else if (numStr.length > 8) {
      let decimal = numStr.substring(numStr.length - 8, numStr.length - 8 + point);
      return Math.floor(num / 100000000) + '.' + decimal + '亿'
    }
  }

  function createAcquisitionData(
    acquiredAt: string,
    companyID: string,
    companyName: string,
    categoryCode: string,
    priceAmount: number
  ) {
    return { acquiredAt, companyID, companyName, categoryCode, priceAmount };
  }

  const [findAcquisitionListQueryResult, executeFindAcquisitionListQuery] = useFindAcquisitionListQuery({
    variables: {
      first: 10,
      after: newAfter,
      companyID: Base64.decode(props.id),
    },
    pause: false,
  });

  useEffect(() => {
    setTimeout(() => {
      setNewAfter((page - 1) * 10);
    }, 0);
  }, [page]);

  useEffect(() => {
    let nodes =
      findAcquisitionListQueryResult.data
        ?.QueryExternalAcquisitionListByCompanyID?.nodes;
    let res = nodes?.map((value) => {
      let newAcquisitionInfo = createAcquisitionData(
        value?.acquiredAt as string,
        value?.companyInfo?.companyID as string,
        value?.companyInfo?.companyName as string,
        value?.companyInfo?.categoryCode as string,
        value?.priceAmount as number
      );
      return newAcquisitionInfo;
    });

    while (res?.length as number < 5) {
      res?.push(createAcquisitionData("", "", "", "", 0))
    }

    while (res?.length as number > 5 && res?.length as number < 10) {
      res?.push(createAcquisitionData("", "", "", "", 0))
    }
    
    setRows(res as AcquisitionInfo[]);
    let r =
      findAcquisitionListQueryResult.data
        ?.QueryExternalAcquisitionListByCompanyID;

    if (r === null) {
      setIsExist(false)
    }
    let totalCount =
      findAcquisitionListQueryResult.data?.QueryExternalAcquisitionListByCompanyID
        ?.totalCount;
    setRounds(totalCount);
    setTotalPageCount(Math.ceil(totalCount / 10));
  }, [findAcquisitionListQueryResult]);

  return (
    <div>
      <Grid item xs={12} className={isExist === true ? classes.container_acquisition : classes.noDisplay}>
        <Grid item xs={12} className={classes.acquisition}>
          <Box className={classes.financing_title}>对外收购</Box>
          <Box
            className={classes.financing_total_amount}
            display="flex"
            flexDirection="row"
          >
            <Box className={classes.amount1}>
              共
              <span
                style={{ color: "#333", marginRight: "3px", marginLeft: "3px" }}
              >
                {rounds}
              </span>
              笔收购
            </Box>
          </Box>
          <TableContainer component={Paper} style={{ boxShadow: "none" }}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead style={{ background: "#FAFAFA" }}>
                <TableRow>
                  <TableCell className={classes.investment_founded_at_style}>
                    日期
                  </TableCell>
                  <TableCell
                    className={classes.investment_object_style}
                    align="left"
                  >
                    收购对象
                  </TableCell>
                  <TableCell
                    className={classes.company_category_style}
                    align="left"
                  >
                    企业类别
                  </TableCell>
                  <TableCell
                    className={classes.investment_financing_type_style}
                    align="right"
                  >
                    收购金额
                  </TableCell>
                  <TableCell
                    style={{ width: "800px" }}
                    align="right"
                  ></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {rows?.map((row, index) => (
                  <TableRow key={row.acquiredAt}>
                    <TableCell component="th" scope="row">
                      {row.acquiredAt}
                    </TableCell>
                    <TableCell align="left">
                      <LinktoCompanyPage
                        companyID={row.companyID}
                        companyName={row.companyName as string}
                      ></LinktoCompanyPage>
                    </TableCell>
                    <TableCell align="left">{row.categoryCode}</TableCell>
                    <TableCell style={{ width: "192px" }} align="right">
                      {tranNumber(row.priceAmount, 2)}
                    </TableCell>
                    <TableCell
                      style={{ width: "800px" }}
                      align="right"
                    ></TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
            <Box className={classes.pagination}>
              <Pagination
                count={totalPageCount}
                page={page}
                shape="rounded"
                onChange={handleChange}
                siblingCount={1}
                boundaryCount={1}
              />
            </Box>
          </TableContainer>
        </Grid>
      </Grid>
    </div>
  );
}
